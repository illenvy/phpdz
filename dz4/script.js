const input = document.querySelector('#text');

document.querySelectorAll('.calc-button').forEach(button => {
    button.addEventListener("click", event => {
        input.value += event.target.textContent;
        // Свойство event.target содержит элемент, на котором сработало событие.
        // Это не тот элемент, к которому был привязан обработчик этого события, а именно самый глубокий тег, 
        // на который непосредственно был, к примеру, совершен клик.
    
        event.preventDefault();
    });
})

document.querySelector('.zeroing').addEventListener("click", event => {
    input.value = "";

    event.preventDefault();
});