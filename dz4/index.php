<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script src="script.js" defer></script>
    <title>Коршунов Даниил 211-322</title>
</head>
<body>
    <header>
        <img class="logo" width="20%" height="20%" src="logo.png" alt="mospolytech">
        <h1 class="header__title">Calculator</h1>
    </header>
    <main>
        <?php
            $inp = $_POST['subject'];
            $result = "";
            if($inp == ""){
                echo "<p class='nothing'>Вы ничего не ввели!</p>";
            }
        ?>
        <form class="calculator" method="POST">
            <input class="type" name="subject" value="<?=$result ?>" type="text" id="text">
            <button class="calc-button">1</button>
            <button class="calc-button">2</button>
            <button class="calc-button">3</button>
            <button class="calc-button">4</button>
            <button class="calc-button">5</button>
            <button class="calc-button">6</button>
            <button class="calc-button">7</button>
            <button class="calc-button">8</button>
            <button class="calc-button">9</button>
            <button class="calc-button zero">0</button>
            <button class="calc-button">(</button>
            <button class="calc-button">)</button>
            <button class="calc-button">+</button>
            <button class="calc-button">-</button>
            <button class="calc-button">/</button>
            <button class="calc-button">*</button>
            <button class="zeroing">Обнулить</button>
            <button class="calc">Рассчитать</button>
        </form>
    </main>
    <footer>
        <p>
            Front: сверстать обычный калькулятор. Числа от 0 до 9, скобка открывающаяся и закрывающаяся, операции: сложение, вычитание, деление, умножение. Поле для отображение пользовательского ввода и выведения результата. Кнопка обнуления. Кнопка выполнения расчета. Поле отображения заполнять с помощью Js. Отправлять на сервер пользовательский ввод как POST-параметр запроса.
        </p>
        <p>
            Back: Проверить на правильность введенных данных. Полученное выражение вычислить с помощью рекурсивного вызова пользовательских функций для указанных операций. Вывести результат в поле отображения (использовать GET параметр запроса).
        </p>
    </footer>
</body>
</html>