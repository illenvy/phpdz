<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Korshunov Daniil 211-322</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <img class="logo" width="20%" height="20%" src="logo.png" alt="mospolytech">
        <h1 class="header__title">Feedback form</h1>
    </header>

    <main>
        <form action="https://httpbin.org/post" method="POST">
            <label for="name_id">Имя</label>
            <input type="text" name="name" id="name_id">
            <label for="email_id">E-mail пользователя</label>
            <input type="email" name="email" id="email_id">
            <label for="select_id">Тип обращения</label>
            <select name="select" id="select_id">
                <option value="Жалоба">Жалоба</option>
                <option value="Предложение">Предложение</option>
                <option value="Благодарность">Благодарность</option>
            </select> 
            <label for="appeal_id">Текст обращения</label>
            <input type="text" name="appeal" id="appeal_id">
            <p>Вариант ответа</p>
            <div class="variant-wrapper"><label for="SMS">SMS</label>
            <input type="checkbox" value="SMS" name="variant" id="SMS">
            <label for="e-mail">E-mail</label>
            <input type="checkbox" value="e-mail" name="variant" id="e-mail"></div>
            <input type="submit"></input>
            <a href="getheaders.php">Ссылка на 2 страницу</a>
        </form>
    </main>

    <footer>
        <p>Собрать сайт из двух страниц.</p>
    </footer>
</body>
</html>