<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Equation</title>
</head>
<body>
    <header>
        <img class="logo" width="20%" height="20%" src="logo.png" alt="mospolytech">
        <h1 class="header__title">Feedback form</h1>
    </header>
    <main>
        <div class="result">
            <?php

                // Схоже с работой Самойлова Владимира, так как думали над задачей вместе в дискорде

                $equation = "X * 9 = 56";
                $x = "X";
                $secondNum = "9";
                $res = "56";
                $posx = strripos($equation, $x);
                $multiply = "*";
                $plus = "+";
                $minus = "-";
                $divide = "/";
                $posmultiply = (boolean)strripos($equation, $multiply);
                $posplus = (boolean)strripos($equation, $plus);
                $posminus = (boolean)strripos($equation, $minus);
                $posdivide = (boolean)strripos($equation, $divide);

                if ($posmultiply === true) {
                    echo "В уравнении $equation есть оператор ($multiply)"."<br>";
                    echo "$x найдено в $equation в позиции $posx".'<br>';
                    echo "Значение переменной X = ";
                    echo $res / $secondNum;
                } else if ($posminus === true) {
                    echo "В уравнении $equation есть оператор ($minus)"."<br>";
                    echo "$x найдено в $equation в позиции $posx".'<br>';
                    echo "Значение переменной X = ";
                    echo $res + $secondNum;
                } else if ($posdivide === true) {

                    if ($posx != 0) {
                        echo "В уравнении $equation есть оператор ($divide)"."<br>";
                        echo "$x найдено в $equation в позиции $posx".'<br>';
                        echo "Значение переменной X = ";
                        echo $secondNum / $res;
                    } else {
                        echo "В уравнении $equation есть оператор ($divide)"."<br>";
                        echo "$x найдено в $equation в позиции $posx".'<br>';
                        echo "Значение переменной X = ";
                        echo $res * $secondNum;
                    }
                    
                } else if ($posplus === true) {
                    echo "В уравнении $equation есть оператор ($plus)"."<br>";
                    echo "$x найдено в $equation в позиции $posx".'<br>';
                    echo "Значение переменной X = ";
                    echo $res - $secondNum;
                }
            ?>
        </div>
        <img src="diagram.png" alt="">
    </main>
    <footer>
        <p>
            Написать программу для решения заданного уравнения. Которая будет определять оператор в заданном уравнении и расположение неизвестной переменной. Находить значение переменной. Нарисовать блок-схему алгоритма работы программы. 
        </p>
    </footer>
</body>
</html>